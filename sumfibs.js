fibsRecursive = (n) => n <= 1 ? n : fibsRecursive(n - 1) + fibsRecursive(n - 2)
sumReducer = (arr) => arr.reduce((acc, elem) => acc + elem)

function sumFibs(num) {
  let counter = 0;
  let fibs = [];

  while (true) {
    let candidate = fibsRecursive(counter)
    if (candidate > num) {
      break
    } else {
      fibs.push(candidate);
      counter++;
    }
  }

  return sumReducer( fibs.filter(x => x % 2 === 1));
}

console.log(sumFibs(6));
