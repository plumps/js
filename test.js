const increment = (function () {
    return function (number, value=1) {
        return number + value;
    };
})();

console.log(increment(5))
console.log(increment(5, 2))