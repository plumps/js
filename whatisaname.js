// my solution

function whatIsInAName(collection, source) {
  // What's in a name?
  var arr = [];
  // Only change code below this line
  arr = collection.filter(item => {
    for (let key of Object.keys(source)) {
      if (source[key] !== item[key]) {
        return false
      }
    }
    return true
  })

  // Only change code above this line
  return arr;
}



console.log(whatIsInAName([{ first: "Romeo", last: "Montague" }, { first: "Mercutio", last: null }, { first: "Tybalt", last: "Capulet" }], { last: "Capulet" }));

// more functional solution

function whatIsInAName2(collection, source) {
  // "What's in a name? that which we call a rose
  // By any other name would smell as sweet.”
  // -- by William Shakespeare, Romeo and Juliet
  var srcKeys = Object.keys(source);

  // filter the collection
  return collection.filter(function (obj) {
    return srcKeys
      .map(function (key) {
        return obj.hasOwnProperty(key) && obj[key] === source[key];
      })
      .reduce(function (a, b) {
        return a && b;
      });
  });
}

// test here
whatIsInAName2(
  [
    { first: "Romeo", last: "Montague" },
    { first: "Mercutio", last: null },
    { first: "Tybalt", last: "Capulet" }
  ],
  { last: "Capulet" }
);