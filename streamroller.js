const def = x => typeof x !== "undefined"; // let defined = 'I am defined'; def(defined) => true

const isArray = x => Array.isArray(x);

const flatten = ([x, ...xs]) =>
  def(x)
    ? isArray(x)
      ? [...flatten(x), ...flatten(xs)]
      : [x, ...flatten(xs)]
    : [];

function steamrollArray(arr) {
  return flatten(arr);
}

let testcase = [1, 2, [3, [[4]]]];

console.log(steamrollArray(testcase));
