function myReplace(str, before, after) {
    return before
        .charAt(0)
        .match(/^[A-Z]/)
        ? str.replace(before, after.replace(/\w/, c => c.toUpperCase()))
        : str.replace(before, after);
}

console.log(myReplace("A quick brown fox jumped over the lazy dog", "jumped", "leaped"));
console.log(myReplace("He is Sleeping on the couch", "Sleeping", "sitting"))
