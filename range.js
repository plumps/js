// 1. for-loop

function range(start, end) {
    var ans = [];
    for (let i = start; i <= end; i++) {
        ans.push(i);
    }
    return ans;
}

// 2. recursive solution

function range2(start, end) {
    // base case
    if (start === end) return [start];
    // recursion
    return [start, ...range2(start + 1, end)];
}

// 3. new Array
function range3(start, end) {
    return Array(end - start + 1)
            .fill(undefined)
            .map((_, idx) => idx + start);
}

// 3a new Array with Array.from
function range3a(start, end) {
    const length = end - start;

    return Array
            .from({length}, (_, idx) => start + idx);
}

// 4. async with yield
function* range4(start, end) {
    for (let i = start; i <= end; i++) {
        yield i;
    }
}

// 5. async with yield - recursive
function* range5(start, end) {
    yield start;
    if (start === end) return;
    yield* range(start + 1, end);
}


// Action

for (i of range3a(10, 23)) {
    console.log(i);
}
