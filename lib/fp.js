const head = ([x]) => x; // head([1, 2, 3, 4]) => 1
const tail = ([, ...xs]) => xs; // tail([1, 2, 3, 4]) => [2, 3, 4]
const def = x => typeof x !== "undefined"; // let defined = 'I am defined'; def(defined) => true
const undef = x => !def(x);
const copy = arr => [...arr];
const length = ([x, ...xs], len = 0) => (def(x) ? length(xs, len + 1) : len);
const reverse = ([x, ...xs]) => (def(x) ? [...reverse(xs), x] : []);
const first = ([x, ...xs], n = 1) =>
  def(x) && n ? [x, ...first(xs, n - 1)] : []; // returns the first n-elements of an array

const last = (xs, n = 1) => reverse(first(reverse(xs), n)); // returns the last n-elements of an array
const slice = ([x, ...xs], i, y, curr = 0) =>
  def(x)
    ? curr === i
      ? [y, x, ...slice(xs, i, y, curr + 1)]
      : [x, ...slice(xs, i, y, curr + 1)]
    : []; // returns a new array with value inserted at given index
const isArray = x => Array.isArray(x);
const flatten = ([x, ...xs]) =>
  def(x)
    ? isArray(x)
      ? [...flatten(x), ...flatten(xs)]
      : [x, ...flatten(xs)]
    : [];
const swap = (a, i, j) =>
  map(a, (x, y) => {
    if (y === i) return a[j];
    if (y === j) return a[i];
    return x;
  });
const map = ([x, ...xs], fn) => (def(x) ? [fn(x), ...map(xs, fn)] : []);
const filter = ([x, ...xs], fn) =>
  def(x) ? (fn(x) ? [x, ...filter(xs, fn)] : [...filter(xs, fn)]) : [];
const reject = ([x, ...xs], fn) => {
  if (undef(x)) return [];
  if (!fn(x)) {
    return [x, ...reject(xs, fn)];
  } else {
    return [...reject(xs, fn)];
  }
};
const partition = (xs, fn) => [filter(xs, fn), reject(xs, fn)];
const reduce = ([x, ...xs], fn, memo, i = 0) =>
  def(x) ? reduce(xs, fn, fn(memo, x, i), i + 1) : memo;
const reduceRight = (xs, fn, memo) => reduce(reverse(xs), fn, memo);
const partial = (fn, ...args) => (...newArgs) => fn(...args, ...newArgs);
const spreadArg = fn => (...args) => fn(args);
const pluck = (key, object) => object[key];
