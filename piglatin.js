// function translatePigLatin(str) {
//   const rxVowelStart = /^[aeiou]/; 
//   const rxConsonantsStart = /^[b-df-hj-np-tv-z]+/;
//   const rxAnyVowels = /[aeiou]/g;

//   // 1. starts with a vowel
//   if (str.match(rxVowelStart)) {
//     return str + "way"
//   }

//   // 2. has no vowel
//   if (!str.match(rxAnyVowels)) {
//     return str + "ay"
//   }

//   // 3. move the consonants to the end
//   let consonants = str.match(rxConsonantsStart)[0]
//   return str.substr(consonants.length) + consonants + "ay"

// }

function translatePigLatin(str) {
    let consonantRegex = /^[^aeiou]+/;
    let myConsonants = str.match(consonantRegex);
    return myConsonants !== null
      ? str
          .replace(consonantRegex, "")
          .concat(myConsonants)
          .concat("ay")
      : str.concat("way");
  }
  
  
  console.log(translatePigLatin("california"));
  console.log(translatePigLatin("algorithm"))
  console.log(translatePigLatin("schwartz"))