function destroyer(arr, ...killer) {
    // Remove all the values
    return arr.filter(item => !killer.includes(item));
  }
  
  console.log(destroyer([1, 2, 3, 1, 2, 3], 2, 3));
  