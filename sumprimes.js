// Sei s die zu testende Zahl
// Sei f=2 der erste Faktor der getestet wird
// Wenn f|s dann ist s nicht prim
// Andernfalls f=3
// Wenn f|s dann ist s nicht prim
// Andernfalls erhöhe f um 2, solange f  Wurzel aus s
// s ist prim

function isPrime(candidate) {
  let factor = 2;

  if (candidate === 3 || candidate === 2){
    return true
  }
  
  if (candidate % factor === 0) {
    return false
  } else {
    factor = 3;
  }

  do {
    if (candidate % factor === 0) return false;
    factor += 2;
  } while (factor <= Math.sqrt(candidate));

  return true;
}

function sumReducer(arr) {
  return arr.reduce((acc, elem) => acc + elem);
}

function sumPrimes(max) {
  let number = 2;
  let primes = [];

  while (true) {
    if (number > max) {
      break;
    }
    if (isPrime(number)) {
      primes.push(number);
    }
    number++;
  }
  return sumReducer(primes);
}

console.log(sumPrimes(11));
console.log(isPrime(8));
